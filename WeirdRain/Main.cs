﻿using GTA;
using System;
using System.Collections.Generic;

namespace WeirdRain
{
    public class WeirdRain : Script
    {
        int tickSpeed;

        List<Thing> spawnedThings = new List<Thing>();

        public WeirdRain()
        {
            var thingTypes = new List<Thing.ThingType>();
            thingTypes.AddRange((Thing.ThingType[])Enum.GetValues(typeof(Thing.ThingType)));

            tickSpeed = Math.Max(Math.Min(Settings.GetValue("SETTINGS", "TICK_RATE", 750), 5000), 100);
            if (!Settings.GetValue("RAIN", "CARS", true)) thingTypes.Remove(Thing.ThingType.Car);
            if (!Settings.GetValue("RAIN", "CATS", true)) thingTypes.Remove(Thing.ThingType.Cat);
            if (!Settings.GetValue("RAIN", "DOGS", true)) thingTypes.Remove(Thing.ThingType.Dog);
            if (!Settings.GetValue("RAIN", "FISH", true)) thingTypes.Remove(Thing.ThingType.Fish);
            if (!Settings.GetValue("RAIN", "STRIPPERS", true)) thingTypes.Remove(Thing.ThingType.Stripper);

            if (thingTypes.Count == 0)
            {
                Interval = 1000;
                Tick += AllThingsDisabled_Tick;
            }
            else
            {
                Thing.RandThingsChooseFrom = thingTypes.ToArray();

                Aborted += WeirdRain_Aborted;
                Interval = tickSpeed;
                Tick += WeirdRain_Tick;
            }
        }

        private void AllThingsDisabled_Tick(object sender, EventArgs e)
        {
            if (Game.IsLoading || Game.IsScreenFadedOut || Game.IsScreenFadingIn) return;

            UI.Notify("Weird Rain: You have all rain types disabled.");

            Tick -= AllThingsDisabled_Tick;
        }

        private void WeirdRain_Aborted(object sender, EventArgs e)
        {
            foreach (Thing thing in spawnedThings)
            {
                thing.Delete();
            }

            spawnedThings.Clear();
        }

        private void processSpawnedThings()
        {
            //delete first thing in array when at max spawned things
            if (spawnedThings.Count > 30)
            {
                if (spawnedThings[0].Exists()) spawnedThings[0].Delete();

                spawnedThings.RemoveAt(0);
            }

            //kill peds when they hit the ground or aren't ragdolled anymore (otherwise you'll have a bunch of peds just standing there)
            for (int i = 0; i < spawnedThings.Count; i++)
            {
                if (spawnedThings[i].type != Thing.ThingType.Car) //is a ped
                {
                    Ped ped = (Ped)spawnedThings[i].thing;

                    if (ped.Exists())
                    {
                        if (!ped.IsAlive || ped.HeightAboveGround > 2f || ped.IsRagdoll) continue;
                        ped.Kill();
                    }
                    else
                    {
                        spawnedThings.RemoveAt(i);
                        i--;
                    }
                }

                Yield();
            }
        }

        private void WeirdRain_Tick(object sender, EventArgs e)
        {
            Ped plrPed = Game.Player.Character;

            if (plrPed.Exists() && plrPed.IsAlive)
            {
                var currentWeather = World.Weather;

                if (currentWeather == Weather.Raining || currentWeather == Weather.ThunderStorm)
                {
                    processSpawnedThings();

                    //spawn weird stuff
                    Thing thing = Thing.GetRandomThing();

                    if (thing.Spawn(plrPed))
                    {
                        spawnedThings.Add(thing);
                    }
                }
            }
            else
            {
                if (spawnedThings.Count != 0)
                {
                    //delete all spawn peds
                    foreach (Thing thing in spawnedThings)
                    {
                        if (thing.Exists())
                        {
                            thing.Delete();
                        }

                        Yield();
                    }

                    spawnedThings.Clear();
                }
            }
        }
    }
}
