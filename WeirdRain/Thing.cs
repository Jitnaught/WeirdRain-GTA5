﻿using GTA;
using GTA.Math;
using GTA.Native;
using System;
using System.Diagnostics;

namespace WeirdRain
{
    class Thing
    {
        public static ThingType[] RandThingsChooseFrom;

        public enum ThingType
        {
            Car,
            Cat,
            Dog,
            Fish,
            Stripper
        }

        static string[] dogModels = { "a_c_chop", "a_c_husky", "a_c_poodle", "a_c_pug", "a_c_retriever", "a_c_rottweiler", "a_c_shepherd", "a_c_westy" },
            fishModels = { "a_c_dolphin", "a_c_fish", "a_c_humpback", "a_c_killerwhale", "a_c_sharkhammer", "a_c_sharktiger", "a_c_stingray" },
            carModels = { "ambulance", "policeold2", "akuma", "wolfsbane", "blade", "ratloader", "bfinjection", "premier", "airbus", "futo", "btype", "airtug", "taco" },
            stripperModels = { "s_f_y_stripper_01", "s_f_y_stripper_02", };

        static Random randType = new Random();
        Random rand = new Random();

        public object thing = null;
        public ThingType type;

        public Thing(ThingType type)
        {
            this.type = type;
        }

        public bool Spawn(Ped plrPed)
        {
            var velOffset = Vector3.Zero;

            if (plrPed.IsInVehicle())
            {
                var veh = plrPed.CurrentVehicle;

                if (veh.Exists() && veh.IsAlive)
                {
                    velOffset = veh.Velocity * 3f;
                }
            }

            var spawnPos = plrPed.GetOffsetInWorldCoords(new Vector3(0f, 0f, 60f)).Around(rand.Next(5, 20)) + velOffset;

            Model model = null;
            if (type == ThingType.Car) model = carModels[rand.Next(0, carModels.Length)];
            else if (type == ThingType.Cat) model = "a_c_cat_01";
            else if (type == ThingType.Dog) model = dogModels[rand.Next(0, dogModels.Length)];
            else if (type == ThingType.Fish) model = fishModels[rand.Next(0, fishModels.Length)];
            else if (type == ThingType.Stripper) model = stripperModels[rand.Next(0, stripperModels.Length)];

            Entity entity = null;
            
            if (type == ThingType.Car) entity = World.CreateVehicle(model, spawnPos);
            else entity = World.CreatePed(model, spawnPos);

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            while (!entity.Exists() && stopwatch.ElapsedMilliseconds < 500) Script.Yield();

            stopwatch.Stop();

            if (entity.Exists())
            {
                thing = entity;

                if (type != ThingType.Car)
                {
                    Ped ped = (Ped)entity;

                    ped.Money = 0;
                    ped.Health = 1;

                    ped.CanRagdoll = true;
                    Function.Call(Hash.SET_PED_TO_RAGDOLL, ped, -1, -1, 0, false, false, false);
                }

                entity.ApplyForce(new Vector3((float)rand.NextDouble() * 2 - 1, (float)rand.NextDouble() * 2 - 1, -50f), new Vector3((float)rand.NextDouble() * 2 - 1, (float)rand.NextDouble() * 2 - 1, (float)rand.NextDouble() * 2 - 1));

                entity.MarkAsNoLongerNeeded();

                return true;
            }

            return false;
        }

        public void Delete()
        {
            Entity entity = (Entity)thing;

            if (entity.Exists())
            {
                for (int i = 255; i > 5; i -= 5)
                {
                    entity.Alpha = i;
                    Script.Yield();
                }

                entity.Delete();
            }
        }

        public bool Exists()
        {
            return (thing as Entity).Exists();
        }

        public static Thing GetRandomThing()
        {
            return new Thing(RandThingsChooseFrom[randType.Next(0, RandThingsChooseFrom.Length)]);
        }
    }
}
