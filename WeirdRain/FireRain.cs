﻿using GTA;
using GTA.Math;
using GTA.Native;
using System.Diagnostics;

namespace WeirdRain
{
    class FireRain //not used as it doesn't work as well as i want it to
    {
        public Entity entity;
        public int ptfx;

        public FireRain(Vector3 pos)
        {
            entity = World.CreateProp("prop_old_wood_chair", pos, true, false);

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            
            while (!entity.Exists() && stopwatch.ElapsedMilliseconds < 500) Script.Yield();

            stopwatch.Stop();

            if (entity.Exists())
            {
                entity.IsVisible = false;
                entity.HasCollision = false;

                Function.Call(Hash.REQUEST_NAMED_PTFX_ASSET, "scr_exile2");
                Function.Call(Hash._SET_PTFX_ASSET_NEXT_CALL, "scr_exile2");
                ptfx = Function.Call<int>(Hash.START_PARTICLE_FX_LOOPED_ON_ENTITY, "scr_ex2_jeep_engine_fire", entity, 0f, 1.5f, 0.5f, 0f, 0f, 0f, 1f, 0, 0, 0);

                entity.MarkAsNoLongerNeeded();
            }
        }

        public void Delete()
        {
            if (entity.Exists())
            {
                entity.Delete();
                entity = null;
            }

            Function.Call(Hash.REMOVE_PARTICLE_FX, ptfx, 0);
            ptfx = 0;
        }
    }
}
